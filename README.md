# README #

React Native Stripe API

### What is this repository for? ###

* This Library is used create payment method by using Publishable key

### How do I get set up? ###

#### Installation 
##### npm install rnative-stripe-api --save
#### Setup 
```JavaScript
import stripe from 'rnative-stripe-api'
const rnStripe = stripe('your publishable key')
const requestData = {
            "type": "card",
            "billing_details": {
                "email": "youruser@example.com",
                "name": "nameoncard",
            },
            "card": {
                "exp_month": "expireMonth",
                "exp_year": "expireYear",
                "number": "cardNumber"
            }
        }
let paymentMethod = rnStripe.createPaymentMethod(requestData)
submitPaymentMethodToserver(paymentMethod)
```
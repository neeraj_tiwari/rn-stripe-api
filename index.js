import formurlencoded from 'form-urlencoded';
import axios from 'axios'
const STRIPE_BASE_URL = 'https://api.stripe.com/v1/';

module.exports = function(key) {
    return {
        createPaymentMethod: async function (requestData) {
            const keys = Object.keys(requestData);
            const index = _findRootIndex(requestData, keys);
            let response;
            if (index == 0) {
                let type = keys[index];
                let newRequestData = _convertRequestData(type, requestData[type]);
                response = await _createPaymentMethodAPI(newRequestData, key);
                return _parseResponse(response);
            } else {
                response = await _createPaymentMethodAPI(requestData, key);
                return _parseResponse(response);
            }
        }
    }
}


function _findRootIndex(requestData, keys) {
    if (requestData.card != null) {
        return keys.indexOf("card");
    } else if (requestData.bank_account != null) {
        return keys.indexOf("bank_account");
    } else if (requestData.pii != null) {
        return keys.indexOf("pii");
    } else return false;
}

function _convertRequestData(type, requestData) {
    let convertedRequestData = {}
    for (let data in requestData) {
        const string = type + '[' + data + ']';
        convertedRequestData[string] = requestData[data];
    }
    return convertedRequestData;
}


async function _parseResponse(response) {
    if (response.data == null) {
        return response;
    } else {
        const body = await response.data;
        return body;
    }
}

async function _createPaymentMethodAPI(requestData, key) {
    const requestBody = formurlencoded(requestData);
    const response = await axios.post(STRIPE_BASE_URL + 'payment_methods', requestBody, {headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + key
        }})
    return response
}